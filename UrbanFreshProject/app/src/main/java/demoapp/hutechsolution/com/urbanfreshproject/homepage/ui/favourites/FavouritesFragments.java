package demoapp.hutechsolution.com.urbanfreshproject.homepage.ui.favourites;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import demoapp.hutechsolution.com.urbanfreshproject.R;

public class FavouritesFragments extends Fragment {


    public static FavouritesFragments newInstance() {
        return new FavouritesFragments();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.favourites_fragment, container, false);
    }

}