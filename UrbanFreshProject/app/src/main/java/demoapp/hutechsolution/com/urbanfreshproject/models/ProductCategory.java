package demoapp.hutechsolution.com.urbanfreshproject.models;

public class ProductCategory {//POJO class for ProductCategory

    private String mCategoryId;
    private String mCategoryName;
    private String mCategoryPicture;

    public ProductCategory(String mCategoryId, String mCategoryName, String mCategoryPicture) {
        this.mCategoryId = mCategoryId;
        this.mCategoryName = mCategoryName;
        this.mCategoryPicture = mCategoryPicture;
    }

    public String getmCategoryId() {
        return mCategoryId;
    }

    public String getmCategoryName() {
        return mCategoryName;
    }

    public String getmCategoryPicture() {
        return mCategoryPicture;
    }
}
