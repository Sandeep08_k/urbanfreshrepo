package demoapp.hutechsolution.com.urbanfreshproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import demoapp.hutechsolution.com.urbanfreshproject.R;
import demoapp.hutechsolution.com.urbanfreshproject.models.ProductCategory;
import demoapp.hutechsolution.com.urbanfreshproject.models.Products;
import demoapp.hutechsolution.com.urbanfreshproject.utils.UrlList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {
    Context mContext;
    List<Products> mCategoryList;

    public ProductAdapter(Context mContext, List<Products> mCategoryList){
        this.mContext=mContext;
        this.mCategoryList=mCategoryList;

    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(mContext).inflate(R.layout.item_products,null,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.mProductName.setText(mCategoryList.get(position).getmProductName());
        holder.mProductPrice.setText(mCategoryList.get(position).getmProductPrice());
        Glide.with(mContext)
                .load(mCategoryList.get(position).getmProductImage())
                .into(holder.mProductImage);

    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView mProductImage;
        TextView mProductName;
        TextView mProductPrice;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mProductName=itemView.findViewById(R.id.xProductName);
            mProductImage=itemView.findViewById(R.id.xProductImage);
            mProductPrice=itemView.findViewById(R.id.xProductPrice);

        }
    }
}
