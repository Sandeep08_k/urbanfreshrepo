package demoapp.hutechsolution.com.urbanfreshproject.splashscreen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import demoapp.hutechsolution.com.urbanfreshproject.R;
import demoapp.hutechsolution.com.urbanfreshproject.homepage.HomePage;

public class SplashScreen extends AppCompatActivity {

    private static int SPLASH_TIMEOUT=2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeSplashScreen();

    }

    private void initializeSplashScreen(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent homepage=new Intent(SplashScreen.this, HomePage.class);
                startActivity(homepage);
                finish();
            }
        },SPLASH_TIMEOUT);

    }
}
