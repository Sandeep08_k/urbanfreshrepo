package demoapp.hutechsolution.com.urbanfreshproject.homepage;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.util.ArrayList;
import java.util.List;

import demoapp.hutechsolution.com.urbanfreshproject.R;

public class HomePage extends AppCompatActivity {
    Spinner mSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_cart,R.id.navigation_favourite, R.id.navigation_account)
                .build();//Initializing bottom navigation elements.
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);//Initializing navigation controller of the activity.
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        getSupportActionBar().hide();//Hides default actionbar
        initializeSpinner();

    }

    private void initializeSpinner() {
        mSpinner=findViewById(R.id.xPlaceList);
        List<String> mplaces=new ArrayList<>();
        mplaces.add("Bommanahalli");
        mplaces.add("BTM");
        mplaces.add("Koramangala");
        ArrayAdapter<String> arrayAdapter=new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,mplaces);
        mSpinner.setAdapter(arrayAdapter);
    }

}
