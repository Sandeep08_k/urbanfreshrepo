package demoapp.hutechsolution.com.urbanfreshproject.homepage.ui.explore;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.json.JSONObject;

import java.util.HashMap;

import demoapp.hutechsolution.com.urbanfreshproject.network.NetworkCall;
import demoapp.hutechsolution.com.urbanfreshproject.utils.UrlList;

public class ExploreViewModel extends ViewModel {

    private MutableLiveData<JSONObject> mProducts;
    private Context mContext;

    public LiveData<JSONObject> getProductDetails(Context mContext){//LiveData is a just a data type that notifies its observer when data is changed.
        this.mContext=mContext;
        if(mProducts==null){
            mProducts=new MutableLiveData<JSONObject>();
            fetchDataFromServer();
        }

        return mProducts;
    }

    private void fetchDataFromServer() {

        try{
            NetworkCall networkCall=new NetworkCall(mContext);
            HashMap<String, String> hmap=new HashMap<>();
            hmap.put("category_id","5");
            networkCall.makePostRequest(UrlList.HOMEPAGE_CONTENTS, new NetworkCall.VolleyCallBack() {
                @Override
                public void onSuccess(JSONObject response) {
                    try{
                        if(response!=null){
                            mProducts.postValue(response);//Post data on background thread.

                        }


                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                }
            },hmap);





        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


}