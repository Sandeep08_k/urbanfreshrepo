package demoapp.hutechsolution.com.urbanfreshproject.network;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import demoapp.hutechsolution.com.urbanfreshproject.utils.UrlList;

public class NetworkCall {
    private Context mContext;
    RequestQueue requestQueue;
    String baseurl;


    public interface VolleyCallBack{//Interface to collect data when network call is success.

        public void onSuccess(JSONObject response);
    }

    public NetworkCall(Context mContext){
        this.mContext=mContext;
        requestQueue= Volley.newRequestQueue(mContext);
    }

    public void makePostRequest(String url, final VolleyCallBack volleyCallBack, final Map<String, String> params){//Making POST request
        baseurl= UrlList.BASE_URL;
        url=baseurl +url;

        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    if(response!=null) {
                        JSONObject res = new JSONObject(response);
                        volleyCallBack.onSuccess(res);
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                    Log.e("Exception",e.getMessage());
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try{
                            Log.e("Volley error",error.getMessage());
                            //volleyCallBack.onSuccess(new JSONObject("{status:4}"));

                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                }

        ){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> p=new HashMap<>();
                p=params;
                return p;
            }
        };
        VolleySingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(stringRequest); //Ensures that single instance of the request queue is maintained and it can used during entire lifetime of App.
    }


}
