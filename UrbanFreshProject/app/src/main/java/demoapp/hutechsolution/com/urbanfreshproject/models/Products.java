package demoapp.hutechsolution.com.urbanfreshproject.models;

public class Products {//POJO class for Products
    private int mProductImage;
    private String mProductName;
    private String mProductPrice;

    public Products(int mProductImage, String mProductName, String mProductPrice) {
        this.mProductImage = mProductImage;
        this.mProductName = mProductName;
        this.mProductPrice = mProductPrice;
    }

    public int getmProductImage() {
        return mProductImage;
    }

    public String getmProductName() {
        return mProductName;
    }

    public String getmProductPrice() {
        return mProductPrice;
    }
}
