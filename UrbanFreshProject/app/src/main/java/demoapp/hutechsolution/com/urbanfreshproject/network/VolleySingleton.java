package demoapp.hutechsolution.com.urbanfreshproject.network;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleySingleton {
    private RequestQueue mRequestQueue;
    private  static VolleySingleton mInstance;

    private static Context context;

    private VolleySingleton(Context mContext){
        context=mContext;
        mRequestQueue=getRequestQueue();
    }


    public static synchronized VolleySingleton getInstance(Context context){
        if(mInstance==null){
            mInstance=new VolleySingleton(context);
        }
        return mInstance;
    }


    public RequestQueue getRequestQueue(){
        if(mRequestQueue==null) {
            mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());

            mRequestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {

                @Override
                public void onRequestFinished(Request<Object> request) {
                    mRequestQueue.getCache().clear();
                }
            });
        }
        return mRequestQueue;

    }


    public void addToRequestQueue(Request res){

        getRequestQueue().add(res);
    }



}
