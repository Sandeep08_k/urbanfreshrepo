package demoapp.hutechsolution.com.urbanfreshproject.homepage.ui.explore;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import demoapp.hutechsolution.com.urbanfreshproject.R;
import demoapp.hutechsolution.com.urbanfreshproject.adapters.CategoryAdapter;
import demoapp.hutechsolution.com.urbanfreshproject.adapters.ProductAdapter;
import demoapp.hutechsolution.com.urbanfreshproject.models.ProductCategory;
import demoapp.hutechsolution.com.urbanfreshproject.models.Products;
import demoapp.hutechsolution.com.urbanfreshproject.utils.UrlList;


public class ExploreFragment extends Fragment {
    RecyclerView mDailyNeedsProductList;
    RecyclerView mNewArrivalsProductList;
    GridView mCategoryGrid;
    List<ProductCategory> mProductCategoryList;
    List<Products> mProductList;
    Context context;
    ImageView mBannerImage;
    ImageView mAdsBanner;
    ImageView mAdsBanner2;
    ImageView mAdsBanner3;

    private ExploreViewModel exploreViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        exploreViewModel =
                ViewModelProviders.of(this).get(ExploreViewModel.class);//Initializing view model


        View root = inflater.inflate(R.layout.fragment_home, container, false);

        initViews(root);
        context=container.getContext();
        final ProgressDialog progressDialog=new ProgressDialog(container.getContext());
        progressDialog.setMessage("Loading results...");
        progressDialog.show();
        Log.e("Invocation started","true");
        exploreViewModel.getProductDetails(container.getContext()).observe((LifecycleOwner) container.getContext(), new Observer<JSONObject>() {
            @Override
            public void onChanged(JSONObject jsonObject) {//Observing the data
                try{

                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                    Log.e("Network res----",jsonObject.toString());
                    JSONArray jsonArray=jsonObject.getJSONArray("components");
                    setUpProductAdapter();
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject1=jsonArray.getJSONObject(i);

                        if(jsonObject1.toString().contains("categorydata")){
                            JSONArray categorylist=jsonObject1.getJSONArray("categorydata");
                            setUpProductCategoryAdapter(categorylist);
                            Log.e("---CategoryData---",categorylist.toString());
                        }


                        else if(jsonObject1.toString().contains("StaticBanner")){
                             JSONArray bannerimage=jsonObject1.getJSONArray("StaticBanner");
                             Glide.with(context)
                                    .load(UrlList.BASE_IMAGE_URL+bannerimage.getJSONObject(0).getString("banner_image"))
                                    .into(mBannerImage);

                        }

                        else if(jsonObject1.toString().contains("AdsBanner")){
                            JSONArray adsbanner=jsonObject1.getJSONArray("AdsBanner");
                            Glide.with(context)
                                    .load(UrlList.BASE_IMAGE_URL+adsbanner.getJSONObject(0).getString("banner_image"))
                                    .into(mAdsBanner);
                            Glide.with(context)
                                    .load(UrlList.BASE_IMAGE_URL+adsbanner.getJSONObject(0).getString("banner_image"))
                                    .into(mAdsBanner2);
                            Glide.with(context)
                                    .load(UrlList.BASE_IMAGE_URL+adsbanner.getJSONObject(0).getString("banner_image"))
                                    .into(mAdsBanner3);



                        }

                    }

                }
                catch (Exception e){
                    e.printStackTrace();
                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return root;
    }

    private void setUpProductCategoryAdapter(JSONArray categorylist) {//Setting up product category adapter
        try{
            if(categorylist.length() >0){
                for(int j=0;j<categorylist.length();j++){
                    JSONObject category=categorylist.getJSONObject(j);
                    String category_id=String.valueOf(category.getString("category_id"));
                    String category_name=category.getString("category_name");
                    String category_pic=category.getString("category_picture");

                    mProductCategoryList.add(new ProductCategory(category_id,category_name,category_pic));
                }
                Log.e("Product list size", String.valueOf(mProductCategoryList.size()));
                CategoryAdapter categoryAdapter=new CategoryAdapter(context,mProductCategoryList);
                mCategoryGrid.setAdapter(categoryAdapter);

            }


        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private void initViews(View root) {//Initializing views
        mDailyNeedsProductList=root.findViewById(R.id.xEFDailyNeedsList);
        mNewArrivalsProductList=root.findViewById(R.id.xEFNewArrivalsList);
        mCategoryGrid=root.findViewById(R.id.xEFMainCategoryList);
        mProductCategoryList=new ArrayList<>();
        mProductList=new ArrayList<>();
        mBannerImage=root.findViewById(R.id.xEFBannerImage1);
        mAdsBanner=root.findViewById(R.id.xEFBannerImage2);
        mAdsBanner2=root.findViewById(R.id.xEFBannerImage3);
        mAdsBanner3=root.findViewById(R.id.xEFBannerImage5);


    }
    private void setUpProductAdapter() {//Setting up product adapter

        mDailyNeedsProductList.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
        mNewArrivalsProductList.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
        mDailyNeedsProductList.setHasFixedSize(true);
        mNewArrivalsProductList.setHasFixedSize(true);
        ProductAdapter productAdapter=new ProductAdapter(context,addProducts());
        mDailyNeedsProductList.setAdapter(productAdapter);
        mNewArrivalsProductList.setAdapter(productAdapter);
    }




    private List<Products> addProducts() {
            mProductList.add(new Products(R.drawable.tomato_pic,"Tomato","₹12.50"));
            mProductList.add(new Products(R.drawable.ic_ooty_potato,"Ooty Potato","₹32.50"));
            mProductList.add(new Products(R.drawable.ic_grapes,"Organic Brinjal","₹64.00"));
            mProductList.add(new Products(R.drawable.ic_orange,"Tomato","₹30.00"));
            mProductList.add(new Products(R.drawable.ic_grapes,"Carrot","₹34.00"));
            return mProductList;

    }
}
