package demoapp.hutechsolution.com.urbanfreshproject.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import demoapp.hutechsolution.com.urbanfreshproject.R;
import demoapp.hutechsolution.com.urbanfreshproject.models.ProductCategory;
import demoapp.hutechsolution.com.urbanfreshproject.utils.UrlList;

public class CategoryAdapter extends BaseAdapter{
    private Context mContext;
    private List<ProductCategory> productCategoryList;
    private LayoutInflater inflater;

    public CategoryAdapter(Context mContext, List<ProductCategory> productCategoryList) {
        this.mContext = mContext;
        this.productCategoryList = productCategoryList;
        inflater=(LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return productCategoryList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view1;
        if(view==null) {
            view1=new View(mContext);
            view1 = inflater.inflate(R.layout.item_productcategory,viewGroup,false);
        }
        else {
            view1=(View) view;
        }
        ImageView pcimage = view1.findViewById(R.id.xPCCategoryImage);
        TextView pctext = view1.findViewById(R.id.PCCategoryName);
        pctext.setText(productCategoryList.get(i).getmCategoryName());
        Glide.with(mContext)
                .load(UrlList.BASE_IMAGE_URL+productCategoryList.get(i).getmCategoryPicture())
                .into(pcimage);



        return view1;
    }
}
